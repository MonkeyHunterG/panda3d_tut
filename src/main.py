from math import pi, cos, sin

from panda3d.core import loadPrcFile
from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor

loadPrcFile("../cfg/conf.prc")


class MyGame(ShowBase):
    def __init__(self):
        super().__init__()

        # Load model data
        self.scene = self.loader.loadModel("models/environment")
        self.scene.reparentTo(self.render)
        self.scene.setScale(.25, .25, .25)
        self.scene.setPos(-8, 42, 0)

        # Task that runs every frame
        self.taskMgr.add(self.spin_camera_task, "SpinCameraTask")

        # Load actor data
        self.panda = Actor("models/panda-model",
                           {"walk": "models/panda-walk4"})
        self.panda.setScale(0.005, 0.005, 0.005)
        self.panda.reparentTo(self.render)
        self.panda.loop("walk")

        # Task that runs every frame
        self.taskMgr.add(self.move_panda_task, "MovePandaTask")

    def spin_camera_task(self, task):
        angle_degrees = task.time
        angle_radians = angle_degrees * (pi / 180.0)
        self.camera.setPos(20 * sin(angle_radians), -20 * cos(angle_radians), 3)
        self.camera.setHpr(angle_degrees, 0, 0)
        return Task.cont

    def move_panda_task(self, task):
        self.panda.setPos(0, -task.time, 0)
        return Task.cont

game = MyGame()
game.run()
